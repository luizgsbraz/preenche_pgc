# Instalar Ultimo GeckoDriver

```
./install_gecko.sh
```

# Prepare Ambiente

```bash
cd $HOME/GIT/folha_de_ponto
python3 -m venv py3env
source py3env/bin/activate
pip install -U pip setuptools
pip install -r requirements.txt
```

# Ajusta Variáveis

# Preenche a Planilha

Conforme orientações em ./originais/README.md

# Executa

```bash
python preenche.py
```
