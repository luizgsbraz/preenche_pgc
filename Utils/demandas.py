#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import re
import sys
import time

from Utils.log import create_error_msg

from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
# from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import Select
# from selenium.webdriver.support.ui import WebDriverWait
# from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
# from selenium.common.exceptions import StaleElementReferenceException
# from selenium.common.exceptions import NoSuchElementException
# from selenium.common.exceptions import TimeoutException

def insereDemandaMJ(dados, logger, browser):
    URL_BASE = 'https://novo.formularios.mj.gov.br/index.php/646385'
    URL_CLEAN = URL_BASE + '/lang/pt-BR/newtest/Y'
    FORM_TITLE = 'PLANO ANUAL DE CONTRATAÇÕES PAC 2023 PF'


    # logger.info("Inserindo a demanda: " + str(demanda))
    logger.info("Inserindo a demanda " + dados[1])
    logger.info(dados)

    try:
        browser.get(URL_CLEAN)
        if browser.title != FORM_TITLE:
            raise Exception("Formulário não encontrado")

        # Página 0
        num_page = 1
        logger.info("Abre Página: " + str(num_page))
        browser.get(URL_BASE)
        if browser.title != FORM_TITLE:
            raise Exception("Página inicial não encontrada")

        logger.info("Submit " + str(num_page))
        xpath = '//*[@id="ls-button-submit"]'
        submit_bt = browser.find_element(by=By.XPATH, value=xpath)
        submit_bt.click()

        # Página 1
        num_page = num_page + 1
        logger.info("Abre Página: " + str(num_page))

        # Campo 1
        # logger.info("Orgão")
        # xpath = '/html/body/article/div/div[3]/div/div/form/div[2]/div/div[3]/div[3]/div/div/select'
        # unid_req = Select(browser.find_element(by=By.XPATH, value=xpath))
        # unid_req.select_by_value("A2")

        # Campo 2
        logger.info("Campo 2")
        xpath = '/html/body/article/div/div[3]/div/div/form/div[2]/div/div[3]/div[3]/div/div/select'
        unid_req = Select(browser.find_element(by=By.XPATH, value=xpath))
        unid_req.select_by_value("A2")

        # Campo 3
        logger.info("Campo 3")
        xpath = '//*[@id="answer646385X5059X45001"]'
        unid_req = Select(browser.find_element(by=By.XPATH, value=xpath))
        unid_req.select_by_value("A1")

        logger.info("Submit " + str(num_page))
        xpath = '//*[@id="ls-button-submit"]'
        submit_bt = browser.find_element(by=By.XPATH, value=xpath)
        submit_bt.click()

        # Página 2
        num_page = num_page + 1
        logger.info("Abre Página: " + str(num_page))

        # Campo 4
        logger.info("Objeto")
        xpath = '//*[@id="answer646385X5061X45003"]'
        objeto_contrat = browser.find_element(by=By.XPATH, value=xpath)
        objeto_contrat.send_keys(dados[10])

        # Campo 5
        logger.info("Resumo Objeto")
        xpath = '//*[@id="answer646385X5061X45005"]'
        objeto_contrat = browser.find_element(by=By.XPATH, value=xpath)
        objeto_contrat.send_keys(dados[10])

        logger.info("Campo 7")
        xpath = '//*[@id="answer646385X5061X44975"]'
        objeto_contrat = browser.find_element(by=By.XPATH, value=xpath)
        objeto_contrat.send_keys(dados[22])

        logger.info("Campo 8")
        xpath = '//*[@id="answer646385X5061X44983"]'
        objeto_contrat = browser.find_element(by=By.XPATH, value=xpath)
        # objeto_contrat.send_keys("01/01/2022")
        objeto_contrat.send_keys(dados[31])

        logger.info("Campo 9")
        xpath = '//*[@id="answer646385X5061X44977"]'
        unid_req = Select(browser.find_element(by=By.XPATH, value=xpath))
        unid_req.select_by_value("A5")

        logger.info("Campo 10")
        xpath = '//*[@id="answer646385X5061X45021"]'
        objeto_contrat = browser.find_element(by=By.XPATH, value=xpath)
        objeto_contrat.send_keys("10000")

        logger.info("Campo 11")
        xpath = '//*[@id="answer646385X5061X45137"]'
        unid_req = Select(browser.find_element(by=By.XPATH, value=xpath))
        unid_req.select_by_value("A1")

        logger.info("Campo 12")
        xpath = '//*[@id="answer646385X5061X44995"]'
        unid_req = Select(browser.find_element(by=By.XPATH, value=xpath))
        unid_req.select_by_value("A1")

        logger.info("Campo 13")
        xpath = '//*[@id="answer646385X5061X44997"]'
        unid_req = Select(browser.find_element(by=By.XPATH, value=xpath))
        unid_req.select_by_value("A4")

        logger.info("Submit " + str(num_page))
        xpath = '//*[@id="ls-button-submit"]'
        submit_bt = browser.find_element(by=By.XPATH, value=xpath)
        submit_bt.click()

        # Página 3
        num_page = num_page + 1
        logger.info("Abre Página: " + str(num_page))

        logger.info("Campo 14")
        xpath = '//*[@id="answer646385X5063X45029"]'
        unid_req = Select(browser.find_element(by=By.XPATH, value=xpath))
        unid_req.select_by_value("A4")

        logger.info("Campo 15")
        xpath =     '//*[@id="answer646385X5063X45055"]'
        unid_req = Select(browser.find_element(by=By.XPATH, value=xpath))
        unid_req.select_by_value("A4")

        logger.info("Campo 16")
        xpath = '//*[@id="answer646385X5063X45061"]'
        unid_req = Select(browser.find_element(by=By.XPATH, value=xpath))
        unid_req.select_by_value("A191")

        logger.info("Campo 17")
        xpath = '//*[@id="answer646385X5063X44993"]'
        unid_req = Select(browser.find_element(by=By.XPATH, value=xpath))
        unid_req.select_by_value("A2")

        logger.info("Campo 18")
        xpath = '//*[@id="javatbd646385X5063X45009N"]'
        unid_req = browser.find_element(by=By.XPATH, value=xpath)
        unid_req.click()
        # unid_req.select_by_value("A2")

        logger.info("Submit " + str(num_page))
        xpath = '//*[@id="ls-button-submit"]'
        submit_bt = browser.find_element(by=By.XPATH, value=xpath)
        submit_bt.click()

        # Página 5
        num_page = num_page + 1
        logger.info("Abre Página: " + str(num_page))

        logger.info("Campo 20")
        xpath = '//*[@id="answer646385X5065X44987"]'
        unid_req = Select(browser.find_element(by=By.XPATH, value=xpath))
        unid_req.select_by_value("A5")

        logger.info("Campo 21")
        xpath = '//*[@id="javatbd646385X5065X44985N"]'
        unid_req = browser.find_element(by=By.XPATH, value=xpath)
        unid_req.click()

        logger.info("Campo 22")
        xpath = '//*[@id="answer646385X5065X44981"]'
        unid_req = Select(browser.find_element(by=By.XPATH, value=xpath))
        unid_req.select_by_value("A2")

        logger.info("Campo 23")
        xpath = '//*[@id="answer646385X5065X45013"]'
        unid_req = Select(browser.find_element(by=By.XPATH, value=xpath))
        unid_req.select_by_value("A3")

        logger.info("Campo 24")
        xpath = '//*[@id="answer646385X5065X45015"]'
        unid_req = Select(browser.find_element(by=By.XPATH, value=xpath))
        unid_req.select_by_value("A2")

        logger.info("Campo 25")
        xpath = '//*[@id="answer646385X5065X45017"]'
        unid_req = Select(browser.find_element(by=By.XPATH, value=xpath))
        unid_req.select_by_value("A2")

        logger.info("Submit " + str(num_page))
        xpath = '//*[@id="ls-button-submit"]'
        submit_bt = browser.find_element(by=By.XPATH, value=xpath)
        submit_bt.click()

        # Nova Página
        num_page = num_page + 1
        logger.info("Abre Página: " + str(num_page))

        logger.info("Campo 27")
        xpath = '//*[@id="answer646385X5067X44989"]'
        objeto_contrat = browser.find_element(by=By.XPATH, value=xpath)
        # element.send_keys(" and some", Keys.ARROW_DOWN)
        objeto_contrat.send_keys("Jose Da Silva")

        logger.info("Campo 28")
        xpath = '//*[@id="answer646385X5067X44991"]'
        objeto_contrat = browser.find_element(by=By.XPATH, value=xpath)
        objeto_contrat.send_keys("61 999919999")

        logger.info("Campo 29")
        xpath = '//*[@id="answer646385X5067X44999"]'
        objeto_contrat = browser.find_element(by=By.XPATH, value=xpath)
        objeto_contrat.send_keys("noone@nowhere.com")

        raise Exception("Antes do ùltimo submit")

        logger.info("Submit " + str(num_page))
        xpath = '//*[@id="ls-button-submit"]'
        submit_bt = browser.find_element(by=By.XPATH, value=xpath)
        submit_bt.click()

        logger.info("Arquivo processado: " + pgc_fullpath)
        browser.quit()

    except Exception:
        logger.error(create_error_msg(sys.exc_info()))

    finally:
        print('Arquivo Processado: ' +  "nome do arquivo processado")
