#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import pandas as pd

# def getData():
#     _data = {}
#     for i in range(30):
#         _data[i] = 'Val' + str(i)
#
#     return _data


def readDemandas(fullfn,a_sheet,head_ln,valid_ln):
    xls = pd.read_excel(fullfn, sheet_name=a_sheet)
    if xls.empty:
        logger.info('empty')
        exit()

    xlstop = xls.head(head_ln + valid_ln)
    xlstop_rec = xlstop.to_records()

    return(xlstop_rec)
