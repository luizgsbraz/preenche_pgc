#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import logging
import logging.handlers as handlers

# Configura o Logger
def create_logger(_name, _log_dir):
    if not os.path.exists(_log_dir):
        os.makedirs(_log_dir)
# create logger with 'spam_application'

    logger = logging.getLogger(_name)
    logger.setLevel(logging.DEBUG)
    logger.propagate = False
    fh = handlers.RotatingFileHandler(os.path.join(
        _log_dir, 'runtime_%s.log' % _name), maxBytes=500000, backupCount=15)
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    # ch.setLevel(logging.ERROR)
    # create formatter and add it to the handlers
    formatter = logging.Formatter(
        '%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    fh.setFormatter(formatter)
    ch.setFormatter(formatter)
    # add the handlers to the logger
    logger.addHandler(fh)
    logger.addHandler(ch)
    return logger

def create_error_msg(_info, other_data=None):
    exc_type, exc_obj, exc_tb = _info[0], _info[1], _info[2]
    fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
    error_line = exc_tb.tb_lineno
    error_function = os.path.split(exc_tb.tb_frame.f_code.co_name)[1]
#     error_value = exc_tb.tb_next
    if other_data is None:
        error_raised = """ERROR: %s LINE: %s FUNCTION: %s File %s, """\
                       % (exc_obj, error_line, error_function, fname)
    else:
        error_raised = """ERROR: %s LINE: %s FUNCTION: %s File %s, Data: %s"""\
                       % (exc_obj,
                          error_line,
                          error_function,
                          fname,
                          other_data)

    return error_raised
