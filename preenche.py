#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# import argparse
# from datetime import datetime, timedelta

import os
import re
import sys
import time

from numpy.core.numeric import NaN

from selenium import webdriver
from selenium.webdriver.firefox.service import Service
from selenium.webdriver.firefox.options import Options
#
# from selenium.webdriver.common.by import By
# from selenium.webdriver.common.keys import Keys
# # from selenium.webdriver.support import expected_conditions as EC
# from selenium.webdriver.support.ui import Select
# # from selenium.webdriver.support.ui import WebDriverWait
# # from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
# #
# # from selenium.common.exceptions import StaleElementReferenceException
# # from selenium.common.exceptions import NoSuchElementException
# # from selenium.common.exceptions import TimeoutException

from Utils.data import *
from Utils.log import *
from Utils.demandas import *

# -------------------------------------------------------------
# VARIÁVEIS A SEREM AJUSTADAS
# -------------------------------------------------------------
LOCAL_BASE_DIR = "/home/lgsb/GIT/preenche_pgc"

LOG_DIR = LOCAL_BASE_DIR + "/temp"
LOG_FN = "pgc.log"
log_fullpath = LOG_DIR + "/" + LOG_FN


PGC_DIR = LOCAL_BASE_DIR + "/originais"
PGC_FN = "ControleDemandasDRCC2022.xlsx"
pgc_fullpath = PGC_DIR + "/" + PGC_FN
pgc_sheetname = "itens"
ln_cabec = 2
ln_validas = 1

# -------------------------------------------------------------
# VARIÁVEIS A SEREM CONFERIDAS APENAS (Normalmente não muda)
# -------------------------------------------------------------
FIREFOX_PATH = "/usr/bin/firefox-esr"
GECKODRIVER_PATH = "/usr/local/bin/geckodriver"

# -------------------------------------------------------------
# VARIÁVEIS DOS FORMULARIOSd

if __name__ == "__main__" :
    logger = create_logger(LOG_FN, LOG_DIR)
    options = Options()
    options.headless = False
    # options.profile = "./noprofile"
    service = Service(GECKODRIVER_PATH)
    browser = webdriver.Firefox(options=options, service=service)
    demandas = readDemandas(pgc_fullpath,pgc_sheetname,ln_cabec,ln_validas);
    for i in range(ln_cabec-1,ln_cabec+ln_validas):
        insereDemandaMJ(demandas[i], logger, browser)
